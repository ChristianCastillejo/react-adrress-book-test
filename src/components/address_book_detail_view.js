import React from "react";
import { Grid, Button, Label } from "semantic-ui-react";
import styles from "../assets/styles";
const AddressBookView = ({
  contactList,
  selectContact,
  selectedContact,
  handleFieldChange,
  handleView
}) => {
  return (
    <Grid.Column width={10} textAlign="center">
      <Button
        circular
        icon="edit"
        floated="right"
        style={styles.editButton}
        onClick={() => handleView("edit")}
      />
      <p style={styles.detailTitle}>Details</p>
      <Label circular size="massive" style={styles.bigCircle}>
        <p style={styles.pBigCircle}>
          {selectedContact.firstName &&
            selectedContact.firstName.charAt(0).toUpperCase()}
          {selectedContact.lastName &&
            selectedContact.lastName.charAt(0).toUpperCase()}
        </p>
      </Label>
      <p style={styles.detailName}>
        {`${selectedContact.firstName} ${selectedContact.lastName}`}
      </p>
      <Grid style={styles.detailGrid}>
        <Grid.Row columns={2}>
          <Grid.Column>
            <p> First Name:</p>
            <p style={styles.detailField}>{selectedContact.firstName}</p>
          </Grid.Column>
          <Grid.Column style={styles.detailRightColum}>
            <p>Last Name:</p>
            <p style={styles.detailField}>{selectedContact.lastName}</p>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            <p> Email:</p>
            <p style={styles.detailField}>{selectedContact.email}</p>
          </Grid.Column>
          <Grid.Column style={styles.detailRightColum}>
            <p>Country:</p>
            <p style={styles.detailField}>{selectedContact.country}</p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Grid.Column>
  );
};

export default AddressBookView;
