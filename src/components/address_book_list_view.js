import React from "react";
import _ from "lodash";
import { Grid, Button, Divider } from "semantic-ui-react";
import styles from "../assets/styles";
const AddressBookView = ({
  contactList,
  selectContact,
  selectedContact,
  handleView,
  view
}) => {
  return (
    <Grid.Column width={6}>
      <p style={styles.contactListTitle}>
        Contacts
        {view !== "add" && (
          <Button
            circular
            icon="add"
            floated="right"
            style={styles.addButton}
            onClick={() => handleView("add")}
          />
        )}
      </p>
      <div style={styles.contactList}>
        {_.orderBy(contactList, ["firstName"], ["asd"]).map(
          (contact, index) => (
            <div
              key={index}
              onClick={() => selectContact(contact)}
              style={styles.cursor}
            >
              <p style={styles.pList}>
                <span style={styles.bold}> {contact.firstName}</span>{" "}
                {contact.lastName}
              </p>
              <Divider />
            </div>
          )
        )}
      </div>
    </Grid.Column>
  );
};

export default AddressBookView;
